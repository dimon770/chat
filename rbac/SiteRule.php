<?php

namespace app\rbac;

use Yii;
use yii\rbac\Rule;

class SiteRule extends Rule
{
    public $name = 'isCompanySite';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated width.
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['modelSite']) ? $params['modelSite']->company_id == Yii::$app->user->identity->company_id : false;
    }

}