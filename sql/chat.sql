-- MySQL Script generated by MySQL Workbench
-- Пт 10 ноя 2017 16:49:09
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema online_chat
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `online_chat` ;

-- -----------------------------------------------------
-- Schema online_chat
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `online_chat` DEFAULT CHARACTER SET utf8 ;
USE `online_chat` ;

-- -----------------------------------------------------
-- Table `online_chat`.`companies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`companies` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`companies` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Таблица компаний';


-- -----------------------------------------------------
-- Table `online_chat`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`users` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `email` VARCHAR(200) NOT NULL DEFAULT '',
  `password` VARCHAR(255) NOT NULL DEFAULT '',
  `name` VARCHAR(45) NOT NULL DEFAULT '',
  `role` ENUM('root', 'admin', 'operator') NOT NULL DEFAULT 'admin',
  `online` TINYINT(1) NOT NULL DEFAULT 0,
  `auth_key` VARCHAR(255) NOT NULL DEFAULT '',
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_auth` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `company_id_idx` (`company_id` ASC),
  CONSTRAINT `users_company_id`
    FOREIGN KEY (`company_id`)
    REFERENCES `online_chat`.`companies` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'таблица пользователей';


-- -----------------------------------------------------
-- Table `online_chat`.`sites`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`sites` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`sites` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `url` VARCHAR(200) NOT NULL DEFAULT '',
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `company_id_idx` (`company_id` ASC),
  CONSTRAINT `sites_company_id`
    FOREIGN KEY (`company_id`)
    REFERENCES `online_chat`.`companies` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'список сайтов';


-- -----------------------------------------------------
-- Table `online_chat`.`user_sites`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`user_sites` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`user_sites` (
  `user_id` INT UNSIGNED NOT NULL,
  `site_id` INT UNSIGNED NOT NULL,
  INDEX `site_id_idx` (`site_id` ASC),
  PRIMARY KEY (`user_id`, `site_id`),
  CONSTRAINT `user_sites_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `online_chat`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `user_sites_site_id`
    FOREIGN KEY (`site_id`)
    REFERENCES `online_chat`.`sites` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'сайты пользователей';


-- -----------------------------------------------------
-- Table `online_chat`.`sites_settings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`sites_settings` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`sites_settings` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_id` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `site_id_idx` (`site_id` ASC),
  CONSTRAINT `sites_settings_site_id`
    FOREIGN KEY (`site_id`)
    REFERENCES `online_chat`.`sites` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'настройки сайтов';


-- -----------------------------------------------------
-- Table `online_chat`.`clients`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`clients` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`clients` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `name` VARCHAR(45) NOT NULL DEFAULT '',
  `phone` VARCHAR(11) NOT NULL DEFAULT '',
  `email` VARCHAR(100) NOT NULL DEFAULT '',
  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `site_id_idx` (`site_id` ASC),
  CONSTRAINT `clients_site_id`
    FOREIGN KEY (`site_id`)
    REFERENCES `online_chat`.`sites` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'клиенты';


-- -----------------------------------------------------
-- Table `online_chat`.`users_settings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`users_settings` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`users_settings` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `user_id_idx` (`user_id` ASC),
  CONSTRAINT `users_settings_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `online_chat`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'настройки пользователей';


-- -----------------------------------------------------
-- Table `online_chat`.`widgets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`widgets` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`widgets` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `token` VARCHAR(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  INDEX `widgets_site_id_idx` (`site_id` ASC),
  CONSTRAINT `widgets_site_id`
    FOREIGN KEY (`site_id`)
    REFERENCES `online_chat`.`sites` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'виджеты сайтов';


-- -----------------------------------------------------
-- Table `online_chat`.`widgets_settings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`widgets_settings` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`widgets_settings` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `widget_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `window_color` VARCHAR(45) NOT NULL DEFAULT '',
  `font_color` VARCHAR(6) NOT NULL DEFAULT '',
  `message_color` VARCHAR(6) NOT NULL DEFAULT '',
  `position_top` VARCHAR(4) NOT NULL DEFAULT '',
  `position_right` VARCHAR(4) NOT NULL DEFAULT '',
  `position_bottom` VARCHAR(4) NOT NULL DEFAULT '',
  `position_left` VARCHAR(4) NOT NULL DEFAULT '',
  `font_size` VARCHAR(3) NOT NULL DEFAULT '',
  `font_family` VARCHAR(45) NOT NULL DEFAULT '',
  `font_type` VARCHAR(45) NOT NULL DEFAULT '',
  `online_text` VARCHAR(255) NOT NULL DEFAULT '',
  `offline_text` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  INDEX `widget_id_idx` (`widget_id` ASC),
  CONSTRAINT `widgets_widget_id`
    FOREIGN KEY (`widget_id`)
    REFERENCES `online_chat`.`widgets` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'настройки виджета';


-- -----------------------------------------------------
-- Table `online_chat`.`conversations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`conversations` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`conversations` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `room_id` VARCHAR(10) NOT NULL DEFAULT '',
  `status` VARCHAR(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  INDEX `site_id_idx` (`site_id` ASC),
  CONSTRAINT `conversations_site_id`
    FOREIGN KEY (`site_id`)
    REFERENCES `online_chat`.`sites` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'диалоги';


-- -----------------------------------------------------
-- Table `online_chat`.`conversation_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`conversation_users` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`conversation_users` (
  `conversation_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `user_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `client_id` INT UNSIGNED NOT NULL DEFAULT 0,
  INDEX `conversation_id_idx` (`conversation_id` ASC),
  CONSTRAINT `conversation_users_conversation_id`
    FOREIGN KEY (`conversation_id`)
    REFERENCES `online_chat`.`conversations` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'участники диалога';


-- -----------------------------------------------------
-- Table `online_chat`.`conversation_messages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `online_chat`.`conversation_messages` ;

CREATE TABLE IF NOT EXISTS `online_chat`.`conversation_messages` (
  `id` INT UNSIGNED NOT NULL,
  `conversation_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `user_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `client_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `send_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `conversation_id_idx` (`conversation_id` ASC),
  CONSTRAINT `conversation_messages_conversation_id`
    FOREIGN KEY (`conversation_id`)
    REFERENCES `online_chat`.`conversations` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'сообщения диалога';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
