<?php

use yii\helpers\Html;

$this->title                   = 'Онлайн чат';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-md-3">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a data-toggle="pill" href="#home">Диалог 1</a></li>
            <li><a data-toggle="pill" href="#menu1">Диалог 2</a></li>
            <li><a data-toggle="pill" href="#menu2">Диалог 3</a></li>
        </ul>
    </div>
    <div class="col-md-9">
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <h3>HOME</h3>
                <p>Some content.</p>
            </div>
            <div id="menu1" class="tab-pane fade">
                <h3>Menu 1</h3>
                <p>Some content in menu 1.</p>
            </div>
            <div id="menu2" class="tab-pane fade">
                <h3>Menu 2</h3>
                <p>Some content in menu 2.</p>
            </div>
        </div>
    </div>
</div>