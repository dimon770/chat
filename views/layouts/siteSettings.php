<?php
use \yii\helpers\Html;
use \yii\helpers\Url;

$siteId = Yii::$app->request->get('id');
?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<div id="site-settings" class="row" data-site-id="<?= Yii::$app->request->get('id') ?>">
    <div class="col-md-3">
        <div class="background-gray">
            <ul class="site-settings_menu">
                <li>
                    <i class="fa fa-window-maximize"></i>
                    <a href="<?= Url::to(['/admin/site-settings/install', 'id' => $siteId]) ?>">Код для установки виджета</a>
                </li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>
    <div class="col-md-9">
        <?= $content ?>
    </div>
</div>
<?php $this->endContent(); ?>
