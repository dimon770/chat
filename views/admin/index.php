<?php
/*
 *
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use \app\models\User;

$this->title                   = 'панель администратора';
$this->params['breadcrumbs'][] = $this->title;

$userSitesArr = ArrayHelper::toArray(
    $userSites,
    [
        'app\models\User' => [
            'id',
            'email',
            'name',
            'role',

        ],
    ]
);

$companyOperators = ArrayHelper::toArray(
    $companyUsers,
    [
        'app\models\User' => [
            'id',
            'email',
            'name',
            'role',

        ],
    ]
);


?>
<div class="row">
    <div class="col-md-12">
        <ul id="myTabs" class="nav nav-tabs" role="tablist">
            <li class="active">
                <a href="#sites"
                   aria-controls="sites"
                   role="tab"
                   data-toggle="tab">Сайты
                </a>
            </li>
            <li>
                <a href="#operators"
                   aria-controls="operators"
                   role="tab"
                   data-toggle="tab">Операторы
                </a>
            </li>
        </ul>
        <div class="create-items">
            <span class=""
                  data-toggle="modal"
                  data-target="#create-site"
                  title="добавить сайт">
                <i class="fa fa-plus fa-2x"></i>
            </span>
            <span class="p"
                  data-toggle="modal"
                  data-target="#create-operator"
                  title="добавить оператора">
                <i class="fa fa-user-plus fa-2x"></i>
            </span>
        </div>
    </div>
</div>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="sites">
        <div class="row">
            <?php for ($siteNumber = 0; $siteNumber < count($sites); $siteNumber++) {
                ?>
                <div class="col-md-6 block">
                    <div class="background-gray sites_site-block">
                        <h3><?= Html::encode($sites[$siteNumber]->url) ?></h3>
                        <div>
                            <a class="btn btn-default"
                               href="<?= Url::to(
                                   [
                                       '/site-settings/install',
                                       'id' => Html::encode($sites[$siteNumber]->id),
                                   ]
                               ) ?>"
                               role="button">
                                <i class="fa fa-cogs"></i> Настройки
                            </a>
                            <a class="btn"
                               href="">Журнал
                            </a>
                            <a class="btn"
                               href="">Статистика
                            </a>
                            <a class="btn bind-operators"
                               data-toggle="modal"
                               data-target="#bind-operators"
                               data-active-operators='<?= json_encode($userSitesArr[$siteNumber]) ?>'
                               data-site-id='<?= Html::encode($sites[$siteNumber]->id) ?>'
                               data-site-url='<?= Html::encode($sites[$siteNumber]->url) ?>'>Назначить операторов
                            </a>

                            <?= Html::a(
                                '',
                                [
                                    '/admin/delete-site',
                                    'id' => $sites[$siteNumber]->id,
                                ],
                                [
                                    'class'        => 'fa fa-trash-o btn',
                                    'data-confirm' => 'Удалить?',
                                ]
                            );
                            ?>
                        </div>
                        <div>
                            <h4>Операторы:</h4>
                            <p>
                                <?php
                                foreach ($userSitesArr[$siteNumber] as $siteUser) {
                                    ?>
                                    <span class="operator-mini">
                                        <i class="fa fa-user fa-lg"></i>
                                        <?= Html::encode($siteUser['name']) ?>
                                    </span>
                                    <?php
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="operators" data-all-operators='<?= json_encode($companyOperators) ?>'>
        <div class="row">
            <div class="col-md-6 block">
                <table class=" table table-responsive table-hover">
                    <tr>
                        <th>#</th>
                        <th>Имя</th>
                        <th>Роль</th>
                        <th>Почта</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <?php
                    $i = 1;
                    $j = 0;
                    foreach ($companyUsers as $companyUser) { ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= Html::encode($companyUser['name']) ?></td>
                            <td><?= Html::encode($companyUser['role']) ?></td>
                            <td><?= Html::encode($companyUser['email']) ?></td>
                            <td>
                                <span class="settings-items operator-settings"
                                      data-operator-settings='<?= json_encode($companyOperators[$j]) ?>'
                                      title="настройки оператора"
                                      data-toggle="modal"
                                      data-target="#edit-operator">
                                    <i class="fa fa-gear fa-lg"></i>
                                </span>
                            </td>
                            <td>
                                <?= Html::a(
                                    '',
                                    [
                                        '/admin/delete-operator',
                                        'id' => Html::encode($companyUser['id']),
                                    ],
                                    [
                                        'class'        => 'fa fa-trash-o fa-lg',
                                        'data-confirm' => 'Удалить?',
                                    ]
                                );
                                ?>
                            </td>
                        </tr>
                        <?php $i++;
                        $j++;
                    } ?>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- форма создания сайта -->
<div class="modal fade" id="create-site" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Добавление сайта</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(
                    [
                        'action' => ['admin/create-site'],
                        'id'     => 'create-site-form',
                    ]
                ); ?>

                <?= $form->field($siteModel, 'scheme')->dropDownList(
                    ['http://' => 'http://', 'https://' => 'https://']
                )->label('Схема'); ?>


                <?= $form->field($siteModel, 'url', ['enableAjaxValidation' => true])
                         ->textInput()
                         ->label('Сайт') ?>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="create-site-form" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- форма создания сайта END -->


<!-- форма создания оператора -->
<div class="modal fade" id="create-operator" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Новый оператор</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(
                    [
                        'action' => ['admin/create-operator'],
                        'id'     => 'create-operator-form',
                    ]
                ); ?>

                <?= $form->field($userModel, 'name')
                         ->textInput()
                         ->label('Имя')
                ?>

                <?= $form->field(
                    $userModel,
                    'email',
                    [
                        'enableAjaxValidation' => true,
                        'inputOptions'         => [
                            'autocomplete' => 'off',
                            'class'        => 'form-control',
                        ],
                    ]
                )
                         ->input('email')
                         ->label('Почта')
                ?>

                <?= $form->field(
                    $userModel,
                    'password',
                    [
                        'inputOptions' => [
                            'autocomplete' => 'new-password',
                            'class'        => 'form-control',
                        ],
                    ]
                )
                         ->passwordInput()
                         ->label('Пароль')
                ?>
                <?= $form->field($userModel, 'passwordRepeat')->passwordInput()->label('Повторите пароль') ?>
                <?= $form->field($userModel, 'role', ['enableAjaxValidation' => true])
                         ->dropDownList(
                             [
                                 User::ADMIN_ROLE    => 'Администратор',
                                 User::OPERATOR_ROLE => 'Оператор',
                             ],
                             [
                                 'prompt' => 'Выберите один вариант',
                             ]
                         )
                         ->label('Роль')
                ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="create-operator-form" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- форма создания оператора END -->


<!-- форма редактирования оператора -->
<div class="modal fade" id="edit-operator" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Редактирование оператора</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(
                    [
                        'action' => ['admin/edit-operator'],
                        'id'     => 'edit-operator-form',
                    ]
                ); ?>
                <?= $form->field($editUserModel, 'user_id')
                         ->hiddenInput(['value' => ''])->label(false);
                ?>

                <?= $form->field($editUserModel, 'name')
                         ->textInput()
                         ->label('Имя')
                ?>

                <?= $form->field($editUserModel, 'role', ['enableAjaxValidation' => true])
                         ->dropDownList(
                             [
                                 User::ADMIN_ROLE    => 'Администратор',
                                 User::OPERATOR_ROLE => 'Оператор',
                             ],
                             [
                                 'prompt' => 'Выберите один вариант',
                             ]
                         )
                         ->label('Роль')
                ?>

                <?php ActiveForm::end(); ?>
                <a class="" role="button" data-toggle="collapse" href="#change-password-form"
                   aria-expanded="false" aria-controls="change-password-form">
                    Сменить пароль
                </a>
                <?php $form = ActiveForm::begin(
                    [
                        'action'  => ['admin/change-password'],
                        'id'      => 'change-password-form',
                        'options' => [
                            'class' => 'collapse',
                        ],
                    ]
                ); ?>

                <?= $form->field($passwordChangeModel, 'user_id')->hiddenInput(['value' => ''])->label(false); ?>


                <?= $form->field(
                    $passwordChangeModel,
                    'currentPassword',
                    [
                        'enableAjaxValidation' => true,
                        'inputOptions'         => [
                            'autocomplete' => 'new-password',
                            'class'        => 'form-control',
                        ],
                    ]
                )->passwordInput()->label('Текущий пароль') ?>
                <?= $form->field(
                    $passwordChangeModel,
                    'newPassword',
                    [
                        'inputOptions' => [
                            'autocomplete' => 'new-password',
                            'class'        => 'form-control',
                        ],
                    ]
                )->passwordInput()->label('Новый пароль') ?>
                <?= $form->field(
                    $passwordChangeModel,
                    'newPasswordRepeat',
                    [
                        'inputOptions' => [
                            'autocomplete' => 'new-password',
                            'class'        => 'form-control',
                        ],
                    ]
                )->passwordInput()->label(
                    'Повторите пароль'
                ) ?>
                <button type="submit" form="change-password-form" class="btn btn-primary">Change password</button>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="edit-operator-form" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- форма редактирования оператора END -->


<!-- форма привязки операторов к сайту -->
<div class="modal fade" id="bind-operators" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title bind-operator-to-site_title">Операторы</h4>
            </div>
            <div class="modal-body">
                <div class="row clearfix">
                    <?php foreach ($companyUsers as $companyUser) { ?>
                        <div class="operator col-md-2 block"
                             data-operator-id="<?= Html::encode($companyUser['id']) ?>">
                            <i class="fa fa-user fa-4x"></i>
                            <p><?= Html::encode($companyUser['name']) ?></p>
                        </div>
                    <?php } ?>
                </div>
                <?= Html::beginForm(
                    ['admin/bind-operator-to-site'],
                    'post',
                    [
                        'enctype' => 'multipart/form-data',
                        'id'      => 'bind-operators-form',
                    ]
                ) ?>

                <?= Html::hiddenInput('siteId', '') ?>
                <?php
                foreach ($companyUsers as $companyUser) {
                    echo Html::hiddenInput('user[' . $companyUser['id'] . ']', '0');
                }
                ?>

                <?= Html::endForm() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="bind-operators-form" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- форма привязки операторов к сайту END -->