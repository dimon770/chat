<?php

use yii\helpers\Html;

$this->title      = 'Регистрация завершена!';
$registrarionData = [
    'email'    => 'Email',
    'password' => 'Пароль',
    'name'     => 'Имя',
    'site'     => 'Адрес сайта',
];
?>
<h1><?= Html::encode($this->title) ?></h1>
<p>Данные регистрации:</p>
<p>
    <span><b><?= Html::encode($registrarionData['email']) ?>:</b></span>
    <span><?= Html::encode($model->email) ?></span>
</p>
<p>
    <span><b><?= Html::encode($registrarionData['password']) ?>:</b></span>
    <span><?= Html::encode($model->password) ?></span>
</p>
<p>
    <span><b><?= Html::encode($registrarionData['name']) ?>:</b></span>
    <span><?= Html::encode($model->name) ?></span>
</p>
<p>
    <span><b><?= Html::encode($registrarionData['site']) ?>:</b></span>
    <span><?= Html::encode($model->url) ?></span>
</p>
<p><a href="/admin/login" class="btn btn-success">Вход</a></p>
