<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $userModel app\models\User */

/* @var $siteModel app\models\Sites */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title                   = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Пожалуйста, заполните следующие поля для регистрации:</p>

    <?php $form = ActiveForm::begin(
        [
            'id'          => 'registration-form',
            'options'     => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template'     => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]
    ); ?>

    <?= $form->field($userModel, 'email', ['enableAjaxValidation' => true])
             ->textInput(['autofocus' => true]) ?>

    <?= $form->field($userModel, 'password')->passwordInput()->label('Пароль') ?>
    <?= $form->field($userModel, 'passwordRepeat')->passwordInput()->label('Повторите пароль') ?>

    <?= $form->field($userModel, 'name')->textInput()->label('Имя') ?>

    <?= $form->field($siteModel, 'scheme')->dropDownList(
        ['http://' => 'http://', 'https://' => 'https://'],
        ['class' => 'btn btn-default']
    )->label('Схема'); ?>


    <?= $form->field($siteModel, 'url', ['enableAjaxValidation' => true])
             ->textInput()
             ->label('Сайт') ?>


    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton(
                'Зарегистрироваться',
                [
                    'class' => 'btn btn-primary',
                    'name'  => 'registration-button',
                ]
            ) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
