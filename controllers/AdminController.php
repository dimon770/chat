<?php

namespace app\controllers;

use app\models\ChangePasswordForm;
use app\models\Companies;
use app\models\Sites;
use app\models\User;
use app\models\UserSites;
use app\models\LoginForm;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\web\ForbiddenHttpException;

class AdminController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['login'],
                        'roles'   => ['?'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'index',
                            'login',
                            'logout',
                            'create-site',
                            'delete-site',
                            'create-operator',
                            'edit-operator',
                            'delete-operator',
                            'bind-operator-to-site',
                            'change-password',
                        ],
                        'roles'   => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {

        Yii::$app->user->loginUrl = ['admin/login'];

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $companyId    = Yii::$app->user->identity->company_id;
        $company      = Companies::findOne($companyId);
        $sites        = $company->sites;
        $companyUsers = $company->users;

        $siteModel           = new Sites();
        $siteModel->scenario = Sites::SCENARIO_CREATE;
        $users               = $siteModel->getUsersBySite($sites);

        $userModel           = new User();
        $userModel->scenario = User::SCENARIO_CREATE;

        $editUserModel           = new User();
        $editUserModel->scenario = User::SCENARIO_EDIT;

        $changePasswordUserModel           = new User();
        $changePasswordUserModel->scenario = User::SCENARIO_CHANGE_PASSWORD;
        $passwordChangeModel               = new ChangePasswordForm($changePasswordUserModel);


        return $this->render(
            'index',
            [
                'sites'               => $sites,
                'userSites'           => $users,
                'companyUsers'        => $companyUsers,
                'siteModel'           => $siteModel,
                'userModel'           => $userModel,
                'editUserModel'       => $editUserModel,
                'passwordChangeModel' => $passwordChangeModel,
            ]
        );
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        $model           = new LoginForm();
        $model->scenario = LoginForm::SCENARIO_ADMIN_LOGIN;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['admin/index']);
        }

        return $this->render(
            'login',
            [
                'model' => $model,
            ]
        );
    }

    public function actionCreateSite()
    {
        $model           = new Sites();
        $model->scenario = Sites::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            yii::$app->session->setFlash('success', 'Сайт успешно создан!');

            return $this->redirect(Yii::$app->request->referrer);
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }
        yii::$app->session->setFlash('danger', 'Не удалось создать сайт');

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeleteSite($id)
    {
        $model = Sites::findOne($id);
        if (Yii::$app->user->can('deleteSite', ['modelSite' => $model])) {
            if ($model->delete()) {
                yii::$app->session->setFlash('success', 'Сайт успешно удален!');

                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        throw new ForbiddenHttpException(Yii::$app->params['forbiddenHttpExceptionMessage']);
    }

    public function actionCreateOperator()
    {
        $model           = new User();
        $model->scenario = User::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            yii::$app->session->setFlash('success', 'Пользователь успешно создан!');

            return $this->redirect(Yii::$app->request->referrer);
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }
    }

    public function actionEditOperator()
    {
        $request = Yii::$app->request->post('User');
        $model   = User::findOne($request['user_id']);
        if (Yii::$app->user->can('editUser', ['modelUser' => $model])) {
            $model->scenario = User::SCENARIO_EDIT;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                yii::$app->session->setFlash('success', 'Пользователь успешно изменен!');

                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        throw new ForbiddenHttpException(Yii::$app->params['forbiddenHttpExceptionMessage']);
    }

    public function actionDeleteOperator($id)
    {
        $model = User::findOne($id);
        if (Yii::$app->user->can('editUser', ['modelUser' => $model])) {
            if ($model->delete()) {
                yii::$app->session->setFlash('success', 'Пользователь успешно удален!');

                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        throw new ForbiddenHttpException(Yii::$app->params['forbiddenHttpExceptionMessage']);
    }

    public function actionBindOperatorToSite()
    {
        $model = new UserSites();
        if ($model->bindUserToSite(Yii::$app->request->post())) {
            yii::$app->session->setFlash('success', 'Операторы успешно закреплены');

            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionChangePassword()
    {
        $request             = Yii::$app->request->post('ChangePasswordForm');
        $userModel           = User::findOne($request['user_id']);
        $userModel->scenario = User::SCENARIO_CHANGE_PASSWORD;
        $changePasswordModel = new ChangePasswordForm($userModel);

        if (Yii::$app->request->isAjax && $changePasswordModel->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($changePasswordModel);
        }

        if ($changePasswordModel->load(Yii::$app->request->post()) && $changePasswordModel->changePassword()) {
            yii::$app->session->setFlash('success', 'Пароль успешно изменен!');

            return $this->redirect(Yii::$app->request->referrer);
        }
    }
}
