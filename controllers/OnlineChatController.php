<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 01.05.17
 * Time: 10:18
 */

namespace app\controllers;

use app\models\LoginForm;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class OnlineChatController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['login'],
                        'roles'   => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin', 'operator'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
        ];
    }

    public function beforeAction($action)
    {

        Yii::$app->user->loginUrl = ['online-chat/login'];

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->redirect('/online-chat/index');
        }
        $model           = new LoginForm();
        $model->scenario = LoginForm::SCENARIO_OPERATOR_LOGIN;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/online-chat/index']);

        }

        return $this->render(
            'login',
            [
                'model' => $model,
            ]
        );
    }

}