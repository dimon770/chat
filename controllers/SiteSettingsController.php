<?php

namespace app\controllers;

use app\models\Sites;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

class SiteSettingsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'actions'       => ['install'],
                        'roles'         => ['admin'],
                        'matchCallback' => function ($rule, $action) {
                            $model = Sites::findOne(Yii::$app->request->get('id'));

                            return Yii::$app->user->can('siteSettings', ['modelSite' => $model]);
                        },
                    ],
                ],
            ],
        ];
    }

    public $layout = 'siteSettings';

    public function actionInstall()
    {
        return $this->render('install');
    }
}