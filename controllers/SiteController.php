<?php

namespace app\controllers;

use app\models\UserSites;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\bootstrap\ActiveForm;
use app\models\RegistrationForm;
use app\models\Sites;
use app\models\User;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;


class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRegistration()
    {
        $registrationModel   = new RegistrationForm();
        $siteModel           = new Sites();
        $siteModel->scenario = Sites::SCENARIO_REGISTRATION;
        $userModel           = new User();
        $userModel->scenario = User::SCENARIO_REGISTRATION;

        if (
            Yii::$app->request->isAjax &&
            $siteModel->load(Yii::$app->request->post()) &&
            $userModel->load(Yii::$app->request->post())
        ) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ArrayHelper::merge(
                ActiveForm::validate($siteModel),
                ActiveForm::validate($userModel)
            );
        }

        if (!empty(Yii::$app->request->post())) {
            $registrationModel->setAttributes(Yii::$app->request->post('User'), false);
            $registrationModel->setAttributes(Yii::$app->request->post('Sites'), false);
            if (
            $registrationModel->registration()
            ) {
                return $this->render(
                    'registrationCompleted',
                    [
                        'model' => $registrationModel,
                    ]
                );
            }
        }

        return $this->render(
            'registration',
            [
                'siteModel' => $siteModel,
                'userModel' => $userModel,
            ]
        );
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionClientChat()
    {
        return $this->render('clientChat');
    }

    public function actionServerChat()
    {
        return $this->render('serverChat');
    }

    public function actionTest()
    {
        print_r(UserSites::find()->orderBy('user_id DESC')->asArray()->all());
    }
}
