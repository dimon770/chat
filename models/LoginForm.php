<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    const SCENARIO_ADMIN_LOGIN    = 'adminLogin';
    const SCENARIO_OPERATOR_LOGIN = 'operatorLogin';

    public  $email;
    public  $password;
    public  $rememberMe = true;
    private $__user     = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['rememberMe', 'boolean'],
            ['password', 'validateAdminPassword', 'on' => self::SCENARIO_ADMIN_LOGIN],
            ['password', 'validateOperatorPassword', 'on' => self::SCENARIO_OPERATOR_LOGIN],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_ADMIN_LOGIN    => ['email', 'password', 'rememberMe'],
            self::SCENARIO_OPERATOR_LOGIN => ['email', 'password', 'rememberMe'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        $identity = $this->getUser();

        if ($this->validate()) {
            return Yii::$app->user->login(
                $identity,
                $this->rememberMe
                    ? 3600 * 24 * 30 : 0
            );
        }

        return false;
    }

    public function validateAdminPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (
                !$user || !Yii::$app->authManager->checkAccess(
                    $user->id,
                    'adminLogin'
                ) || !$user->validateUserPassword($this->password)
            ) {
                $this->addError($attribute, 'Неправильный логин или пароль.');
            }
        }
    }

    public function validateOperatorPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (
                !$user || !Yii::$app->authManager->checkAccess(
                    $user->id,
                    'operatorLogin'
                ) || !$user->validateUserPassword($this->password)
            ) {
                $this->addError($attribute, 'Неправильный логин или пароль.');
            }
        }
    }

    private function getUser()
    {

        if ($this->__user === false) {
            $this->__user = User::findByEmail($this->email);
        }

        return $this->__user;
    }
}
