<?php

namespace app\models;

use Yii;
use yii\web\HttpException;
use yii\base\Model;

/**
 * Description of RegistrationForm
 *
 * @author dima
 */
class RegistrationForm extends Model
{

    public $email;
    public $password;
    public $passwordRepeat;
    public $name;
    public $scheme;
    public $url;


    public function registration()
    {
        $companyModel   = new Companies();
        $siteModel      = new Sites();
        $userModel      = new User();
        $userSitesModel = new UserSites();
        $widgetModel    = new Widget();

        // validate data before registration
        $siteModel->scenario = Sites::SCENARIO_REGISTRATION;
        $siteModel->setAttributes(
            [
                'scheme' => $this->scheme,
                'url'    => $this->url,
            ]
        );

        $userModel->scenario = User::SCENARIO_REGISTRATION;
        $userModel->setAttributes(
            [
                'email'          => $this->email,
                'password'       => $this->password,
                'passwordRepeat' => $this->passwordRepeat,
                'name'           => $this->name,
            ]
        );

        if (!$siteModel->validate() || !$userModel->validate()) {
            throw new HttpException(503, 'Вы ввели некорректные данные, попробуйте еще раз');
        }


        // create company
        if (!$companyModel->createCompany()) {
            throw new HttpException(503, 'не удалось создать компанию');
        }

        //create site
        $siteModel->scenario = Sites::SCENARIO_CREATE;
        $siteModel->setAttributes(
            [
                'scheme'     => $this->scheme,
                'url'        => $this->url,
                'company_id' => $companyModel->id,
            ]
        );
        if (!$siteModel->save()) {
            throw new HttpException(503, 'не удалось создать сайт');
        }

        //create user
        $userModel->scenario = User::SCENARIO_CREATE;
        $userModel->setAttributes(
            [
                'company_id'     => $companyModel->id,
                'email'          => $this->email,
                'password'       => $this->password,
                'passwordRepeat' => $this->passwordRepeat,
                'name'           => $this->name,
                'role'           => User::ADMIN_ROLE,
            ]
        );

        if (!$userModel->save()) {
            throw new HttpException(503, 'не удалось создать пользователя');
        }

        //create userSites
        $userSitesModel->setAttributes(
            [
                'user_id' => $userModel->id,
                'site_id' => $siteModel->id,
            ]
        );
        if (!$userSitesModel->save()) {
            throw new HttpException(503, 'не удалось привязать пользователя к сайту');
        }

        //create widget
        $widgetModel->setAttributes(['site_id' => $siteModel->id]);
        if (!$widgetModel->save()) {
            throw new HttpException(503, 'не удалось создать виджет');
        }

        return true;
    }
}