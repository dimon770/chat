<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 23.12.17
 * Time: 14:14
 */

/* @var $this \app\models\User */

namespace app\models;

use Yii;
use yii\web\ForbiddenHttpException;

trait UserEventsTrait
{

    /**
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeDelete()
    {
        $LoggedUser    = Yii::$app->user->identity->id;
        $removableUser = $this->id;
        if (parent::beforeDelete()) {
            if ($LoggedUser != $removableUser) {
                $this->deleteUserAssignment();

                return true;
            } else {
                throw new ForbiddenHttpException('Невозможно удалить залогиненного пользователя');
            }
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function beforeValidate()
    {
        if (!Yii::$app->user->isGuest && $this->company_id == null) {
            $this->company_id = Yii::$app->user->identity->company_id;
        }

        return parent::beforeValidate();
    }

    /**
     * @return mixed
     */
    public function afterValidate()
    {
        if ($this->password == null || $this->password == $this->getOldAttribute('password')) {
            $this->password = $this->getOldAttribute('password');
        } else {
            $this->password = $this->generatePassword();
        }

        return parent::afterValidate();
    }

    /**
     * @param $insert
     *
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function beforeSave($insert)
    {

        if (Yii::$app->user->identity) {
            $LoggedUser = Yii::$app->user->identity->id;
            $editUser   = $this->id;
            if ($LoggedUser == $editUser && $this->getOldAttribute('role') != $this->role) {
                throw new ForbiddenHttpException('Нельзя изменить роль у залогиненного пользователя.');
            }
        }
        $this->deleteUserAssignment();

        if ($this->isNewRecord) {
            $this->auth_key = \Yii::$app->security->generateRandomString();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     *
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->createUserAssignment();

        return true;
    }

    /**
     *
     * @return bool
     */
    private function deleteUserAssignment()
    {
        $auth    = Yii::$app->authManager;
        $oldRole = $auth->getRole($this->getOldAttribute('role'));
        if (!$oldRole) {
            return false;
        }
        $auth->revoke($oldRole, $this->id);

        return true;
    }

    /**
     * @return bool
     */
    private function createUserAssignment()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($this->role);
        $auth->assign($role, $this->id);

        return true;
    }

    /**
     *
     * @return string
     */
    private function generatePassword()
    {
        return Yii::$app->getSecurity()->generatePasswordHash($this->password);
    }
}