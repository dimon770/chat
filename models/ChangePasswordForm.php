<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 11.12.17
 * Time: 22:31
 */

namespace app\models;

use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $currentPassword;
    public $newPassword;
    public $newPasswordRepeat;

    /**
     * @var User
     */
    private $_user;

    /**
     * @param User  $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['currentPassword', 'newPassword', 'newPasswordRepeat'], 'required'],
            ['currentPassword', 'currentPassword'],
            ['newPassword', 'string', 'min' => '6', 'max' => 32],
            ['newPassword', 'compare', 'compareAttribute' => 'newPasswordRepeat'],
        ];
    }

    /**
     * @param string $attribute
     * @param array  $params
     */
    public function currentPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->_user->validateUserPassword($this->$attribute)) {
                $this->addError($attribute, 'Введенный пароль не совпадает с текущим');
            }
        }
    }

    /**
     * @return boolean
     */
    public function changePassword()
    {
        if ($this->validate()) {
            $user           = $this->_user;
            $user->password = $this->newPassword;

            return $user->save();
        } else {
            return false;
        }
    }
}