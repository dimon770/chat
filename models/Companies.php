<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii;

/**
 * Description of Company
 *
 * @author dima
 */
class Companies extends ActiveRecord
{

    public static function tableName()
    {
        return 'companies';
    }

    public function createCompany()
    {
        $this->setExpirationDate();
        if ($this->save()) {
            return true;
        }
    }

    private function setExpirationDate()
    {
        return $this->expiration_date = date(
            "Y-m-d H:i:s",
            date('U') + yii::$app->params['trial'] * 24 * 60 * 60
        );

    }

    public function getSites()
    {
        return $this->hasMany(Sites::className(), ['company_id' => 'id']);
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }
}