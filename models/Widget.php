<?php


namespace app\models;

use yii;
use yii\db\ActiveRecord;

class Widget extends ActiveRecord
{
    public static function tableName()
    {
        return 'widgets';
    }

    public function rules()
    {
        return [
            [['site_id', 'token'], 'required'],
        ];
    }

    public function beforeValidate()
    {
        $this->token = hash('md5', $this->site_id);

        return parent::beforeValidate();
    }
}