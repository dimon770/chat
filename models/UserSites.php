<?php


namespace app\models;


use yii\db\ActiveRecord;
use yii\web\HttpException;

class UserSites extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_sites';
    }

    public function rules()
    {
        return [
            [['user_id', 'site_id'], 'required'],
            ['user_id', 'integer'],
            ['site_id', 'integer'],
        ];
    }

    public function bindUserToSite(array $users)
    {
        foreach ($users['user'] as $key => $value) {
            $data = self::findOne(['user_id' => $key, 'site_id' => $users['siteId']]);
            if (($data && $value == '1') || (!$data && $value == '0')) {
                continue;
            }
            if ($data && $value == '0') {
                if ($data->delete()) {
                    continue;
                }
                throw new HttpException(
                    400,
                    'Не получилось удалить привязку пользователя с id ' . $key
                );
            }
            if (!$data && $value = '1') {
                $model             = new UserSites();
                $model->attributes = [
                    'user_id' => $key,
                    'site_id' => $users['siteId'],
                ];
                if ($model->save()) {
                    continue;
                }
                throw new HttpException(
                    400,
                    'Не получилось привязать пользователя с id ' . $key
                );
            }
        }

        return true;
    }

}