<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use \yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    use UserEventsTrait;

    const ADMIN_ROLE               = 'admin';
    const OPERATOR_ROLE            = 'operator';
    const SCENARIO_CREATE          = 'create';
    const SCENARIO_EDIT            = 'edit';
    const SCENARIO_REGISTRATION    = 'registration';
    const SCENARIO_CHANGE_PASSWORD = 'change';

    public $passwordRepeat;

    public static function tableName()
    {
        return 'users';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['company_id', 'email', 'password', 'passwordRepeat', 'name', 'role'], 'required'],
            ['name', 'string', 'min' => 3, 'max' => 20],
            ['email', 'email'],
            ['email', 'unique', 'message' => 'пользователь с таким email уже существует'],
            ['password', 'string', 'min' => '6', 'max' => 32],
            [
                'password',
                'compare',
                'compareAttribute' => 'passwordRepeat',
                'on'               => self::SCENARIO_REGISTRATION,
            ],
            [
                'password',
                'compare',
                'compareAttribute' => 'passwordRepeat',
                'on'               => self::SCENARIO_CREATE,
            ],
            ['role', 'in', 'range' => [self::ADMIN_ROLE, self::OPERATOR_ROLE], 'strict' => false],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE          => [
                'company_id',
                'email',
                'password',
                'passwordRepeat',
                'name',
                'role',
            ],
            self::SCENARIO_EDIT            => ['name', 'role'],
            self::SCENARIO_REGISTRATION    => ['email', 'password', 'passwordRepeat', 'name'],
            self::SCENARIO_CHANGE_PASSWORD => ['password'],
        ];
    }

    /**
     * IdentityInterface
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * IdentityInterface
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
    }

    /**
     * IdentityInterface
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * IdentityInterface
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * IdentityInterface
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return boolean if password is valid for current user
     */
    public function validateUserPassword($password)
    {
        if (Yii::$app->getSecurity()->validatePassword($password, $this->password)) {
            return true;
        }

        return false;
    }

    /**
     * change user authKey when user was logout
     *
     * @param $identity
     *
     * @return bool
     */
    public static function changeAuthKey($identity)
    {
        $user           = self::findOne($identity->identity['id']);
        $user->scenario = self::SCENARIO_EDIT;
        $user->auth_key = \Yii::$app->security->generateRandomString();
        if ($user->save()) {
            return true;
        }
    }

    /**
     * find user by email address
     *
     * @param $email
     *
     * @return bool|static
     */
    public static function findByEmail($email)
    {
        $user = User::findOne(['email' => $email]);

        if ($user) {
            return new static($user);
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSites()
    {
        return $this->hasMany(UserSites::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Sites::className(), ['id' => 'site_id'])
                    ->viaTable('user_sites', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersSettings()
    {
        return $this->hasMany(UsersSettings::className(), ['user_id' => 'id']);
    }

}
