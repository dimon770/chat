<?php


namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\ForbiddenHttpException;

class Sites extends ActiveRecord
{
    const SCENARIO_REGISTRATION = 'registration';
    const SCENARIO_CREATE       = 'create';

    public $scheme;

    public static function tableName()
    {
        return 'sites';
    }

    public function rules()
    {
        return [
            [['scheme', 'url', 'company_id'], 'required'],
            ['scheme', 'in', 'range' => ['http://', 'https://'], 'strict' => false],
            [
                'url',
                'filter',
                'filter' => function ($value) {
                    return $this->scheme . $this->url;
                },
            ],
            ['url', 'url', 'defaultScheme' => 'http'],
            ['url', 'unique', 'message' => 'адрес сайта уже существует'],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_REGISTRATION => ['scheme', 'url'],
            self::SCENARIO_CREATE       => ['scheme', 'url', 'company_id'],
        ];
    }

    /**
     * find site by url address.
     *
     * @param $url
     *
     * @return bool
     */
    public static function findBySite($url)
    {
        $site = self::findOne(['url' => $url]);
        if ($site) {
            return new static($site);
        }

        return false;
    }

    public function beforeValidate()
    {
        if (!Yii::$app->user->isGuest) {
            $this->company_id = Yii::$app->user->identity->company_id;
        }

        return parent::beforeValidate();
    }

    public function getUsersBySite($sites)
    {
        if (!empty($sites)) {
            if (is_array($sites)) {
                foreach ($sites as $site) {
                    $usersArr[] = $site->users;
                }

                return $usersArr;
            }

            return $usersArr[] = $this->users;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Messages::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitesSettings()
    {
        return $this->hasMany(SitesSettings::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSites()
    {
        return $this->hasMany(UserSites::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
                    ->viaTable('user_sites', ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgets()
    {
        return $this->hasMany(Widgets::className(), ['site_id' => 'id']);
    }
}