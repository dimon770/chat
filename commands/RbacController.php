<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 16.04.17
 * Time: 16:43
 */

namespace app\commands;


use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        // user rule
        $userRule = new \app\rbac\UserRule;
        $auth->add($userRule);

        // разрешение "создать сайт"
        $createSite = $auth->createPermission('createSite');
        $createSite->description = 'Создать сайт';
        $auth->add($createSite);

        // разрешение удалить сайт
        $deleteSite = $auth->createPermission('deleteSite');
        $deleteSite->description = 'Удалить сайт';
        $auth->add($deleteSite);

        // разрешение удалить сайт своей компании
        $siteRule = new \app\rbac\SiteRule;
        $auth->add($siteRule);

        $deleteOwnSite = $auth->createPermission('deleteOwnSite');
        $deleteOwnSite->description = 'Удалить свой сайт';
        $deleteOwnSite->ruleName = $siteRule->name;
        $auth->add($deleteOwnSite);

        // разрешение "создать пользователя"
        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Создать пользователя';
        $auth->add($createUser);

        // разрешение редактировать/удалить пользователя
        $editUser = $auth->createPermission('editUser');
        $editUser->description = 'Редактировать/удалить пользователя';
        $auth->add($editUser);

        // разрешение редактировать/удалить своего пользователя
        $editOwnUser = $auth->createPermission('editOwnUser');
        $editOwnUser->description = 'Редактировать/удалить своего пользователя';
        $editOwnUser->ruleName = $userRule->name;
        $auth->add($editOwnUser);

        // разрешение просматривать настройки сайта
        $siteSettings = $auth->createPermission('siteSettings');
        $siteSettings->description = 'доуступ к настройкам сайта';
        $auth->add($siteSettings);

        // разрешение просматривать настройки сайта своей компании
        $ownSiteSettings = $auth->createPermission('ownSiteSettings');
        $ownSiteSettings->description = 'просматривать настройки своего сайта';
        $ownSiteSettings->ruleName = $siteRule->name;
        $auth->add($ownSiteSettings);

        // разрешение на логин администратора
        $adminLogin = $auth->createPermission('adminLogin');
        $adminLogin->description = 'Аутентификация администратора в панеле администратора';
        $auth->add($adminLogin);

        // разрешение на логин оператора
        $operatorLogin = $auth->createPermission('operatorLogin');
        $operatorLogin->description = 'Аутентификация оператора в онлайн чате';
        $auth->add($operatorLogin);


        // разрешения
        $auth->addChild($deleteOwnSite, $deleteSite);
        $auth->addChild($editOwnUser, $editUser);
        $auth->addChild($ownSiteSettings, $siteSettings);
        $auth->addChild($adminLogin, $operatorLogin);

        // добавляем роль admin и ее разрешения
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $createSite);
        $auth->addChild($admin, $deleteOwnSite);
        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $editOwnUser);
        $auth->addChild($admin, $ownSiteSettings);
        $auth->addChild($admin, $adminLogin);

        // добавляем роль operator и ее разрешения
        $operator = $auth->createRole('operator');
        $auth->add($operator);
        $auth->addChild($operator, $operatorLogin);

    }
}