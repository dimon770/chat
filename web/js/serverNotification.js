jQuery(function ($) {
    $(document).ready(function () {

        var
                socket = io.connect('http://localhost:8890'),
                sendMessageClass = $('.send-message');


        socket.on('connect', function () {

        });

        socket.on('notification', function (data) {

            var message = JSON.parse(data);

            $("#notifications").prepend("<p><strong>" + message.name + "</strong>: " + message.message + "</p>");

        });

        sendMessageClass.on('click', function () {
            var
                    chatName = $('#chat-form input[name=name]').val(),
                    chatMessage = $('#chat-form input[name=message]').val(),
                    chatRoomId = $('#chat-form input[name=room-id]').val();

            socket.emit('message', JSON.stringify({'name': chatName, 'message': chatMessage, 'roomId': chatRoomId}));

        });

        socket.on('newClient', function (roomId) {
            socket.emit('room', roomId);
            console.log('new client: ' + roomId);
            $('#chat-form input[name=room-id]').val(roomId);
        });
    });
});