jQuery(function ($) {
    $(document).ready(function () {

        var
                socket = io.connect('http://localhost:8890'),
                socketId,
                roomId,
                sendMessageClass = $('.send-message');

        socket.on('connect', function () {
            socketId = socket.id;
            roomId = 'r' + socketId.substr(1, 4);
            $('#chat-form input[name=room-id]').val(roomId);
            socket.emit('clientConnect', roomId);
            socket.emit('room', roomId);
        });

        sendMessageClass.on('click', function () {
            var
                    chatName = $('#chat-form input[name=name]').val(),
                    chatMessage = $('#chat-form input[name=message]').val(),
                    chatRoomId = $('#chat-form input[name=room-id]').val();
                    
            socket.emit('message', JSON.stringify({'name': chatName, 'message': chatMessage, 'roomId': chatRoomId}));

        });

        socket.on('notification', function (data) {

            var message = JSON.parse(data);
            console.log(data);

            $("#notifications").prepend("<p><strong>" + message.name + "</strong>: " + message.message + "</p>");

        });
    });
});