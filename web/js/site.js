/**
 * Created by dima on 07.03.17.
 */
jQuery(function ($) {
    $(function () {

        var popUpTriggerClass = '.pop-up-trigger', // класс триггера диалоговых окон
            operatorClass = '.operator', // класс оператора в диалоговом окне "назначить операторов"
            operatorIconActiveClass = 'operator-icon_active', // класс активности выбранного оператора в диалоговом окне "назначить операторов"
            operatorInputTrueValue = '1', // значение активности выбранного оператора в диалоговом окне "назначить операторов"
            operatorInputFalseValue = '0', // значение неактивности выбранного оператора в диалоговом окне "назначить операторов"
            OperatorsId = '#operators', // id таблицы операторов
            bindOperatorsClass = '.bind-operators', // класс кнопки "назначить операторов"
            bindOperatorToSiteTitle = '.bind-operator-to-site_title', // тайтл диалогового окна "назначить операторов"
            operatorSettingsClass = '.operator-settings', // класс настроек кнопки "настройки оператора"
            editFormId = '#edit-operator-form',
            changePasswordFormId = '#change-password-form',
            bindOperatorsId = '#bind-operators-form';

        /* main admin page - turn on tabs
         *  включает табы на странице admin/index
         * */
        /* $('#myTabs a').click(function (e) {
         e.preventDefault();
         $(this).tab('show');
         });
         */
        /* pop-up window trigger
         *  отвечает за открытие и закрытие диалоговых окон.
         *  Для работы необходимо добавить на созданное диалоговое окно
         *  класс, а на элемент открытия и закрытие окна data атрибут: data-pop-up="имя класса" с именем этого класса
         * */
        $(popUpTriggerClass).click(function () {
            var dataPopUp = $(this).attr('data-pop-up'),
                popUpClass = $('.' + dataPopUp);

            $(popUpClass).toggle();
        });


        /* set active operators to site
         *  подсвечивает активных операторов сайта при открытии далогового окна привязки операторов к сайту
         * */
        $(bindOperatorsClass).click(function () {
            var activeOperators = $(this).attr('data-active-operators'),
                activeOperatorsObj = JSON.parse(activeOperators),
                siteUrl = $(this).attr('data-site-url'),
                siteId = $(this).attr('data-site-id');

            removeAllOperators();
            $(bindOperatorToSiteTitle).html('Операторы ' + siteUrl);
            $('input[name = "siteId"]').val(siteId);
            for (var operator = 0; operator < activeOperatorsObj.length; operator++) {
                var operatorId = activeOperatorsObj[operator].id;

                changingBindOperator(operatorId);
            }
        });

        $(operatorClass).click(function () {
            var operatorId = $(this).attr('data-operator-id');

            changingBindOperator(operatorId);
        });


        function changingBindOperator(operatorId) {
            console.log(operatorId);
            var operatorIcon = '[data-operator-id = ' + operatorId + '] > i',
                operatorInput = bindOperatorsId + ' input[name = "user[' + operatorId + ']"]';

            if ($(operatorInput).val() === operatorInputTrueValue) {
                $(operatorIcon).removeClass(operatorIconActiveClass);
                $(operatorInput).val(operatorInputFalseValue);
            } else {
                $(operatorIcon).addClass(operatorIconActiveClass);
                $(operatorInput).val(operatorInputTrueValue);
            }
        }

        /* очистка данных активных операторов в диалоговом окне "назначить операторов" */
        function removeAllOperators() {
            var allOperators = $(OperatorsId).attr('data-all-operators');

            $(operatorClass + ' > i').removeClass(operatorIconActiveClass);
            allOperators = JSON.parse(allOperators);
            for (var operator = 0; operator < allOperators.length; operator++) {
                $(bindOperatorsId + ' input[name = "user[' + allOperators[operator].id + ']"]').val(operatorInputFalseValue);
            }

        }

        // установки настроек оператора для выбранного пользователя
        $(operatorSettingsClass).on('click', setOperatorsSettings);

        function setOperatorsSettings() {
            var dataOperator = $(this).attr('data-operator-settings'),
                userId = '[name="User[user_id]"]',
                userName = '[name="User[name]"]',
                userEmail = '[name="User[email]"]',
                userRole = '[name="User[role]"]',
                user = '[name="User[id]"]',
                changePasswordFormInput = '[name="ChangePasswordForm[user_id]"]';

            dataOperator = JSON.parse(dataOperator);
            $(changePasswordFormId + ' ' + changePasswordFormInput).val(dataOperator['id']);
            $(editFormId + ' ' + userId).val(dataOperator['id']);
            $(editFormId + ' ' + userName).val(dataOperator['name']);
            $(editFormId + ' ' + userEmail).val(dataOperator['email']);
            $(editFormId + ' ' + userRole).val(dataOperator['role']);
            $(editFormId + ' ' + user).val(dataOperator['id']);
        }
    });
});