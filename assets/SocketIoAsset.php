<?php


namespace app\assets;

use yii\web\AssetBundle;


class SocketIoAsset extends AssetBundle{
    // Эти файлы не доступны нам из web, так что мы определяем свойство sourcePath.
    // Обратите внимание, что используется алиас @vendor
    public $sourcePath = '@app/nodejs/node_modules/socket.io-client/dist/';
    public $js = [ 'socket.io.js',];
}