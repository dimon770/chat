<?php

return [
    'adminEmail' => 'admin@example.com',
    'trial' => 14,
    'forbiddenHttpExceptionMessage' => 'у вас нет доступа к данной странице или действию',
    'registrationServerExceptionMessage' => 'При регистрации произошла ошибка, попробуйте повторить позднее',
    'registrationClientExceptionMessage' => 'При регистрации произошла ошибка, попробуйте повторить позднее',
];
