<?php

use \app\models\RegistrationForm;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class RegistrationFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }


    /**
     * @dataProvider dataProviderNoValidValues
     */
    public function testRegistrationNoValidValues($email, $password, $passwordRepeat, $name, $scheme, $url)
    {
        $registrationForm = new RegistrationForm(
            [
                'email'          => $email,
                'password'       => $password,
                'passwordRepeat' => $passwordRepeat,
                'name'           => $name,
                'scheme'         => $scheme,
                'url'            => $url,

            ]
        );
        $this->expectException(HttpException::class);
        $registrationForm->registration();
    }

    /**
     * @return array
     */
    public function dataProviderNoValidValues()
    {
        return [
            'noValidEmail'      => ['noValidValue', 'password', 'password', 'name', 'http://', 'site.com'],
            'noValidPassword'   => ['site@site.com', 'pas', 'pas', 'name', 'http://', 'http://site.com'],
            'noComparePassword' => ['site@site.com', 'password', 'password1', 'name', 'http://', 'site.com'],
            'noValidName'       => ['site@site.com', 'password', 'password', 'na', 'http://', 'site.com'],
            'noValidScheme'     => ['site@site.com', 'password', 'password', 'na', 'http', 'site.com'],
            'noValidSite'       => ['site@site.com', 'password', 'password', 'name', 'http://', 'site'],
        ];
    }


    /**
     *
     */
    public function testRegistrationAndDuplicateValues()
    {
        $dataArr = [
            'email'          => 'site@site.com',
            'password'       => 'password',
            'passwordRepeat' => 'password',
            'name'           => 'name',
            'scheme'         => 'http://',
            'url'            => 'site.com',
        ];

        $dateArrEmail     = ['email' => 'site@site.comq'];
        $dateArrSite      = ['url' => 'site.comq'];
        $registrationForm = new RegistrationForm($dataArr);

        expect('registration save', $registrationForm->registration())->true();
        $this->expectException(HttpException::class);

        $registrationForm = new RegistrationForm(ArrayHelper::merge($dataArr, $dateArrSite)); // duplicate email
        $registrationForm->registration();

        $registrationForm = new RegistrationForm(ArrayHelper::merge($dataArr, $dateArrEmail)); // duplicate site
        $registrationForm->registration();

    }

}