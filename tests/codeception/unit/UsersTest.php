<?php

use \app\models\User;

class UsersTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $user;

    protected function _before()
    {
        $this->user = User::find()->orderBy('id DESC')->one();
    }

    protected function _after()
    {
        $this->user = '';
    }


    /**
     * @dataProvider dataProviderUserCreateValues
     *
     */
    public function testCreateUser(
        $companyId,
        $email,
        $password,
        $passwordRepeat,
        $name,
        $role,
        $assertion
    )
    {
        $userModel           = new User();
        $userModel->scenario = User::SCENARIO_CREATE;
        $userModel->setAttributes(
            [
                'company_id'     => $companyId,
                'email'          => $email,
                'password'       => $password,
                'passwordRepeat' => $passwordRepeat,
                'name'           => $name,
                'role'           => $role,
            ]
        );
        expect('create user', $userModel->save())->$assertion();

        if ($assertion == 'true') {
            $this->tester->seeInDatabase(
                'users',
                [
                    'company_id' => $userModel->company_id,
                    'email'      => $userModel->email,
                    'name'       => $userModel->name,
                    'role'       => $userModel->role,
                ]
            );

        }
    }

    /**
     * @dataProvider dataProviderUserEditValues
     *
     */
    public function testEditUser(
        $name,
        $role,
        $assertion
    )
    {
        $userModel           = $this->user;
        $userModel->scenario = User::SCENARIO_EDIT;
        $userModel->setAttributes(
            [
                'name' => $name,
                'role' => $role,
            ]
        );
        expect('edit user', $userModel->save())->$assertion();

        if ($assertion == 'true') {
            $this->tester->seeInDatabase(
                'users',
                [
                    'name'       => $userModel->name,
                    'role'       => $userModel->role,
                ]
            );

        }
    }

    /**
     * @dataProvider dataProviderUserRegistrationValues
     *
     */
    public function testRegistrationUser(
        $email,
        $password,
        $passwordRepeat,
        $name,
        $assertion
    )
    {
        $userModel           = new User();
        $userModel->scenario = User::SCENARIO_REGISTRATION;
        $userModel->setAttributes(
            [
                'email'          => $email,
                'password'       => $password,
                'passwordRepeat' => $passwordRepeat,
                'name'           => $name,
            ]
        );
        expect('registration user', $userModel->validate())->$assertion();
    }

    /**
     * @dataProvider dataProviderChangeUserPassword
     *
     */
    public function testChangeUserPassword($password, $assertion)
    {
        $userModel           = $this->user;
        $userModel->scenario = User::SCENARIO_CHANGE_PASSWORD;
        $userModel->setAttributes(
            [
                'password' => $password,
            ]
        );
        expect('edit user', $userModel->save())->$assertion();
    }

    public function dataProviderUserCreateValues()
    {
        $user = User::find()->orderBy('id DESC')->one();

        return [
            'admin user'          => [
                $user->company_id,
                'qwerty@qwerty.ru',
                'qqqqqqqq',
                'qqqqqqqq',
                'test',
                User::ADMIN_ROLE,
                'true',
            ],
            'operator user'       => [
                $user->company_id,
                'qwerty@qwerty.ruq',
                'qqqqqqqq',
                'qqqqqqqq',
                'test',
                User::OPERATOR_ROLE,
                'true',
            ],
            'no valid email'      => [
                $user->company_id,
                'qwerty',
                'qqqqqqqq',
                'qqqqqqqq',
                'test',
                User::OPERATOR_ROLE,
                'false',
            ],
            'no valid password'   => [
                $user->company_id,
                'qwerty@qwerty.ru',
                'qqq',
                'qqqqqqqq',
                'test',
                User::OPERATOR_ROLE,
                'false',
            ],
            'no compare password' => [
                $user->company_id,
                'qwerty@qwerty.ru',
                'wwwwwwww',
                'qqqqqqqq',
                'test',
                User::OPERATOR_ROLE,
                'false',
            ],
            'no valid name'       => [
                $user->company_id,
                'qwerty@qwerty.ru',
                'qqqqqqqq',
                'qqqqqqqq',
                'qq',
                User::OPERATOR_ROLE,
                'false',
            ],
            'no valid role'       => [
                $user->company_id,
                'qwerty@qwerty.ru',
                'qqqqqqqq',
                'qqqqqqqq',
                'test',
                'qqq',
                'false',
            ],
        ];
    }

    public function dataProviderUserEditValues()
    {
        return [
            'no valid name' => ['1', User::ADMIN_ROLE, 'false'],
            'no valid role' => ['test', 'test', 'false'],
            'valid data'    => ['test', User::ADMIN_ROLE, 'true'],
        ];
    }

    public function dataProviderUserRegistrationValues()
    {
        return [
            'no valid email'      => ['111', 'qqqqqqqq', 'qqqqqqqq', 'test name', 'false'],
            'no valid password'   => ['email@email.com', 'qqq', 'qqq', 'test name', 'false'],
            'no compare password' => [
                'email@email.com',
                'qqqqqqqq',
                'qqqqqqq',
                'test name',
                'false',
            ],
            'no valid name'       => ['email@email.com', 'qqqqqqqq', 'qqqqqqqq', '11', 'false'],
            'valid user'          => [
                'email@email.com',
                'qqqqqqqq',
                'qqqqqqqq',
                'test name',
                'true',
            ],
        ];
    }

    public function dataProviderChangeUserPassword()
    {
        return [
            'empty password'    => ['', 'false'],
            'no valid password' => ['qwe', 'false'],
            'valid password'    => ['qwertyui', 'true'],
        ];
    }
}