<?php

use app\models\UserSites;

class UserSitesTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $userSites;

    protected function _before()
    {
        $this->userSites = UserSites::find()->orderBy('user_id DESC')->asArray()->all();
    }

    protected function _after()
    {
        $this->userSites = '';
    }

    public function testBindUserToSite()
    {
        /*
         *  есть несколько проверок:
         * 1. если пользователь есть и он привяан к сайту, то идем дальше;
         * 2. Если пользователя нет и он не привязан к сайту, то идем дальше;
         * 3. если пользователь есть, но привязку сняли, то удаляем привязку из БД
         * 4. если пользователя нет, но привязка у него активна, то мы добавляем привязку в БД
         *
         *
         *     $usersArr = [
         *         'siteId' => 1,
         *         'user'   => [
         *           '1' => '1',
         *           '2' => '0',
         *           '3' => '1',
         *         ],
         *     ];
         */
        $userArr   = [];
        $userIdVal = '';
        $siteIdVal = '';
        $model     = new UserSites();

        foreach ($this->userSites as $item) {
            //unbind user
            $userIdVal                   = $item['user_id'];
            $siteIdVal                   = $item['site_id'];
            $userArr['siteId']           = $siteIdVal;
            $userArr['user'][$userIdVal] = '0';
            expect($model->bindUserToSite($userArr))->true();
            $this->tester->dontSeeInDatabase(
                'user_sites',
                [
                    'user_id' => $userIdVal,
                    'site_id' => $siteIdVal,
                ]
            );

            //bind user
            $userArr['user'][$userIdVal] = '1';
            expect($model->bindUserToSite($userArr))->true();
            $this->tester->seeInDatabase(
                'user_sites',
                [
                    'user_id' => $userIdVal,
                    'site_id' => $siteIdVal,
                ]
            );


        }

    }
}