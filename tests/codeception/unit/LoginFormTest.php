<?php

use app\models\LoginForm;

class LoginFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    // this users created at test database
    private $userAdmin    = ['email' => 'dima@dima.ru', 'password' => 'dimon770'];
    private $userOperator = ['email' => 'petr@petr.ru', 'password' => 'dimon770'];

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testAdminLoginValidValues()
    {
        $modelLoginForm = new LoginForm($this->userAdmin);

        $modelLoginForm->scenario = LoginForm::SCENARIO_ADMIN_LOGIN;
        expect('login as admin', $modelLoginForm->login())->true();

        $modelLoginForm->scenario = LoginForm::SCENARIO_OPERATOR_LOGIN;
        expect('login as operator', $modelLoginForm->login())->true();
    }

    public function testOperatorLoginValidValues()
    {
        $modelLoginForm = new LoginForm($this->userOperator);

        $modelLoginForm->scenario = LoginForm::SCENARIO_ADMIN_LOGIN;
        expect('login as admin', $modelLoginForm->login())->false();

        $modelLoginForm->scenario = LoginForm::SCENARIO_OPERATOR_LOGIN;
        expect('login as operator', $modelLoginForm->login())->true();
    }

    /**
     * @dataProvider dataProviderNoValidValues
     */
    public function testAdminLoginNoValidValues($email, $password)
    {
        $modelLoginForm = new LoginForm(['email' => $email, 'password' => $password]);

        $modelLoginForm->scenario = LoginForm::SCENARIO_ADMIN_LOGIN;
        expect('login as admin', $modelLoginForm->login())->false();
    }

    /**
     * @dataProvider dataProviderNoValidValues
     */
    public function testOperatorLoginNoValidValues($email, $password)
    {
        $modelLoginForm = new LoginForm(['email' => $email, 'password' => $password]);

        $modelLoginForm->scenario = LoginForm::SCENARIO_OPERATOR_LOGIN;
        expect('login as admin', $modelLoginForm->login())->false();
    }

    public function dataProviderNoValidValues()
    {
        return [
            'login with empty values'                => ['', ''],
            'login with values as string'            => ['qqqqqqqq', 'qqqqqqqq'],
            'login with no registered email'         => ['qwe@qwe.ru', 'qqqqqqqq'],
            'login with incorrect admin password'    => [$this->userAdmin['email'], 'qqqqqqqq'],
            'login with incorrect operator password' => [$this->userOperator['email'], 'qqqqqqqq'],
        ];
    }
}