<?php


class CompaniesTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testCreateCompany()
    {
        $company = new \app\models\Companies();

        expect('create a company', $company->createCompany())->true();
        $this->tester->seeInDatabase('companies', ['expiration_date' => $company->expiration_date]);
        expect(date("Y-m-d H:i:s", date('U')) !== $company->expiration_date)->true(); // now != expiration_date
    }
}