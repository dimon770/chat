<?php

use \app\models\Sites;

class SitesTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $site;

    protected function _before()
    {
        $this->site = Sites::find()->orderBy('id DESC')->one();
    }

    protected function _after()
    {
        $this->site = '';
    }

    /**
     * @dataProvider  dataProviderCreateSiteValues
     */
    public function testCreateSite($scheme, $url, $company_id, $assertion)
    {
        $model           = new Sites(
            ['scheme' => $scheme, 'url' => $url, 'company_id' => $company_id]
        );
        $model->scenario = Sites::SCENARIO_CREATE;

        expect('create site', $model->save())->$assertion();
        if ($assertion == 'true') {
            $this->tester->seeInDatabase(
                'sites',
                [
                    'company_id' => $model->company_id,
                    'url'        => $model->url,
                ]
            );

        }

    }

    /**
     * @dataProvider  dataProviderRegistrationSiteValues
     */
    public function testRegistrationSite($scheme, $url, $assertion)
    {
        $model           = new Sites(['scheme' => $scheme, 'url' => $url]);
        $model->scenario = Sites::SCENARIO_REGISTRATION;

        expect('create site', $model->validate())->$assertion();
    }

    public function dataProviderCreateSiteValues()
    {
        $site = Sites::find()->orderBy('id DESC')->one();

        return [
            'valid site'      => ['http://', 'site.com', $site->company_id, 'true'],
            'no valid scheme' => ['http', 'site.com', $site->company_id, 'false'],
            'no valid url'    => ['http://', 'site', $site->company_id, 'false'],
        ];

    }

    public function dataProviderRegistrationSiteValues()
    {
        return [
            'valid site'      => ['http://', 'site.com', 'true'],
            'no valid scheme' => ['http', 'site.com', 'false'],
            'no valid url'    => ['http://', 'site', 'false'],
        ];
    }
}