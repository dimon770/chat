<?php

use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 07.11.17
 * Time: 8:00
 */
return ArrayHelper::merge(
    require(__DIR__ . '/../../../config/web.php'),
    require(__DIR__ . '/../../../config/testDb.php')
);