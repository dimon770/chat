var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
//var redis = require('redis');

server.listen(8890);

var sockets = {};

io.on('connection', function (socket) {
    console.log("new client connected");

    var
            //redisClient = redis.createClient(),
            currentSocketId = socket.id;

    //redisClient.subscribe('notification');

    /* redisClient.on("message", function (channel, message) {
     console.log("New message: " + message + ". In channel: " + channel);
     json = JSON.parse(message);
     //if (currentSocketId === json.socketId) {
     if (socket.rooms[json.roomId]) {
     socket.in(json.roomId).emit('notification', message);
     }
     //}
     }); */
    socket.on('message', function (message) {
        console.log("New message: " + message);
        json = JSON.parse(message);
        if (socket.rooms[json.roomId]) {
            io.in(json.roomId).emit('notification', message);
        }
    });

    socket.on('clientConnect', function (roomId) {
        console.log('client connect: ' + roomId);
        io.emit('newClient', roomId);
    });

    socket.on('room', function (roomId) {
        socket.join(roomId);
    });

    socket.on('disconnect', function () {
        //redisClient.quit();

    });

});